Single Sign-On (SSO) allows you to link seamlessly to your GTMA portal from another website. Sample code is provided in this repo so you can easily implement this on various platforms.

* [Documentation](https://help.gotomyaccounts.com/customization/implementing-single-sign-on-sso-from-another-website)
* [Video Tutorial](https://www.youtube.com/watch?v=sgffkZJkn5o)



**How to generate Single Sign On (SSO) Tokens**

[![How to generate Single Sign On (SSO) Tokens](https://img.youtube.com/vi/sgffkZJkn5o/0.jpg)](https://www.youtube.com/watch?v=sgffkZJkn5o)
